# PgSQL Backup Service

## Introduction
This project provides a set a systemd files to periodically backup a postgresql server. As stated per
the ![postgresql website](https://www.postgresql.org/docs/current/backup-dump.html) there are 3 ways
for backing up data : SQL dump, filesystem backup and Continuous archiving. This service only uses
SQL dump and filesystem backup.

The main service will try to execute the `pg_dumpall` command, and if the server is down for some 
reason, it will use filesystem archiving and notify the administrator of failure.

## Installation

To install the scripts just use `./install_pgsql_bakcup.sh` and `./delete_pgsql_bakcup.sh` to remove. It will respectively 
copy all .service and .timer files in /usr/lib/systemd/system and then remove them. Change the SERVICE_DIR variable
to suit your installation.

You can change the variables in `pgsql_backup.env` for more flexibility.

