#!/bin/bash
source pgsql_backup.env
sudo cp pgsql_backup.env pgsql_backup_fs.service pgsql_backup.service pgsql_backup.timer $SERVICE_DIR
systemctl daemon-reload
systemctl enable pgsql_backup.service pgsql_backup_fs.service
