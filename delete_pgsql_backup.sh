#!/bin/bash
source pgsql_backup.env
systemctl stop pgsql_backup.service pgsql_backup.timer pgsql_fs_backup.service
systemctl disable pgsql_backup.service pgsql_fs_backup.service
sudo /bin/rm -fv $SERVICE_DIR/{pgsql_backup.env,pgsql_backup_fs.service,pgsql_backup.service,pgsql_backup.timer}
systemctl daemon-reload
